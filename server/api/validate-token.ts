import { createClient } from '@supabase/supabase-js'

export default defineEventHandler(async (event) => {
    const authHeader = event.headers.get('authorization')
    const token = authHeader.split(' ')[1]
    const supabase = createClient('https://pcjsipbqzizxtsfzwuco.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InBjanNpcGJxeml6eHRzZnp3dWNvIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTk3MTM2NTEsImV4cCI6MjAxNTI4OTY1MX0.-2Jl1mUDJxaaOStjV5o1DFZPbwtmkCNtzL3xe0hnnw0')
    const user = await supabase.auth.getUser(token)
    return {
        token,
        user
    }
})