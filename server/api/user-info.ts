import { createClient } from '@supabase/supabase-js'

export default defineEventHandler(async (event) => {
    const supabase = createClient('https://pcjsipbqzizxtsfzwuco.supabase.co', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InBjanNpcGJxeml6eHRzZnp3dWNvIiwicm9sZSI6ImFub24iLCJpYXQiOjE2OTk3MTM2NTEsImV4cCI6MjAxNTI4OTY1MX0.-2Jl1mUDJxaaOStjV5o1DFZPbwtmkCNtzL3xe0hnnw0')

    const authHeader = event.headers.get('authorization')
    const token = authHeader.split(' ')[1]
    if (!token) {
        return {
            error: 'No token provided'
        }
    }
    const user = await supabase.auth.getUser(token)
    await supabase.auth.setSession({access_token: token, refresh_token: token})
    const session = await supabase.auth.getSession()
    console.log(session)

    if (event.method == 'GET') {
        const user_info = await supabase.from('user_info').select('*')

        return {
            user,
            user_info
        }
    }
    else if (event.method == 'POST') {
        const body = await readBody(event)
        const user_info = await supabase.from('user_info')
            .upsert({ id: user.data.user.id, first_name: body.first_name, last_name: body.last_name })
            .select()
        return {
            user,
            user_info
        }
    }
})